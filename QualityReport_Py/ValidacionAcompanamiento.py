#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 15 13:47:17 2025

@author: ricardoortiz
"""

import pandas as pd
import requests
import time



# Cargar el DataFrame inicial (ejemplo)
df = pd.read_table("/Users/ricardoortiz/Documents/Insumos corte trimestral/ValidacionAcompanamiento_Python/BGA Fauna_DwC_Validar-copia.txt", encoding = "utf8", sep= "\t", quoting=3)



#Origen de los datos

Registros = [
    'occurrenceID', 'basisOfRecord', 'institutionCode', 'collectionCode', 'type',
    'institutionID', 'collectionID', 'catalogNumber', 'taxonRank', 'eventDate',
    'country', 'stateProvince', 'county', 'minimumDepth', 'maximumDepth', 
    'minimumElevationInMeters', 'maximumElevationInMeters', 'verbatimLatitude', 
    'verbatimLongitude', 'decimalLatitude', 'decimalLongitude', 'scientificName',
    'kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'specificEpithet'
]

#Eventos = []
#Listas = []

#Ajusar sergún el origen de los datos:  
columns_to_validate = Registros

#Nivel I - Campos obligatorios

# Función para obtener el número de filas documentadas y el porcentaje

def get_completeness(df, columns):
    completeness = {}
    total_rows = len(df)
    for column in columns:
        if column in df.columns:
            # Contar el número de filas no nulas
            documented_rows = df[column].notna().sum()
            # Calcular el porcentaje de completitud
            completeness[column] = {
                'documented_rows': documented_rows,
                'percentage': (documented_rows / total_rows) * 100
            }
    return completeness

# Obtener la completitud
completeness = get_completeness(df, columns_to_validate)

# Crear un DataFrame con los resultados
completeness_df = pd.DataFrame(completeness).T
completeness_df = completeness_df.rename(columns={'documented_rows': 'NO. Filas', 'percentage': 'Porcentaje Completitud'})

# Convertir el índice en una columna
completeness_df.reset_index(inplace=True)
completeness_df = completeness_df.rename(columns={'index': 'ElementoDwC'})

# Mostrar los resultados
print(completeness_df)

# Nivel 2

# Función para obtener valores únicos

def get_unique_values(df, columns, sample_size=5):
    unique_values = {}
    for column in columns:
        if column in df.columns:
            # Obtener los valores únicos
            all_unique_values = df[column].dropna().unique().tolist()
            # Obtener los primeros 'sample_size' valores como muestra
            sample_values = all_unique_values[:sample_size]
            unique_values[column] = {'total_unique': len(all_unique_values), 'sample': sample_values}
    return unique_values

# Obtener valores únicos (muestra de 5 valores por columna)
unique_values = get_unique_values(df, columns_to_validate)

# Crear una lista para almacenar los datos que se usarán para construir el DataFrame
unique_values_list = []

# Agregar el total de valores únicos y los valores únicos de cada columna
for column, data in unique_values.items():
    unique_values_list.append([column, data['total_unique'], data['sample']])

# Crear un DataFrame con los resultados
unique_values_df = pd.DataFrame(unique_values_list, columns=['ElementoDwC', ' No. Valores Únicos', 'Muestra'])

# Mostrar el DataFrame con el total de valores únicos y los valores únicos por columna
print(unique_values_df)




#Nivel III - Taxonómico

# Crear una sesión para reutilizar conexiones
session = requests.Session()

# Función para llamar a la API GBIF y obtener datos
def fetch_gbif_data(session, name, kingdom):
    url = f"http://api.gbif.org/v1/species/match?strict=true&name={name}&kingdom={kingdom}"
    try:
        # Retraso entre solicitudes para evitar sobrecargar el servidor
        time.sleep(0.1)  # 100 ms de retraso
        response = session.get(url, timeout=10)  # Aumentar el timeout a 10 segundos
        if response.status_code == 200:
            return response.json()
        else:
            print(f"Error {response.status_code} for {name}")
            return {}
    except requests.RequestException as e:
        print(f"Error fetching data for {name}: {e}")
        return {}


# Aplicar la función de llamada a la API para llenar una nueva columna 'apiData'
df['apiData'] = df.apply(lambda row: fetch_gbif_data(session, row['scientificName'], row['kingdom']), axis=1)

# Cerrar la sesión después de completar las solicitudes
session.close()




# Extraer y validar campos
def extract_and_validate(row, field, expected_value):
    value = row['apiData'].get(field, None)
    return '1' if value == expected_value else '0'

# Validaciones específicas
df['scientificNameValidation'] = df.apply(lambda row: extract_and_validate(row, 'canonicalName', row['scientificName']), axis=1)
df['genusValidation'] = df.apply(lambda row: extract_and_validate(row, 'genus', row['genus']), axis=1)
df['familyValidation'] = df.apply(lambda row: extract_and_validate(row, 'family', row['family']), axis=1)
df['orderValidation'] = df.apply(lambda row: extract_and_validate(row, 'order', row['order']), axis=1)
df['classValidation'] = df.apply(lambda row: extract_and_validate(row, 'class', row['class']), axis=1)
df['phylumValidation'] = df.apply(lambda row: extract_and_validate(row, 'phylum', row['phylum']), axis=1)
df['kingdomValidation'] = df.apply(lambda row: extract_and_validate(row, 'kingdom', row['kingdom']), axis=1)


# Extraer el nombre validado desde el JSON

df['taxonMatchType'] = df['apiData'].apply(lambda data: data.get('matchType', None))
df['suggestedScientificName'] = df['apiData'].apply(lambda data: data.get('scientificName', None))
df['suggestedCanonicalName'] = df['apiData'].apply(lambda data: data.get('canonicalName', None))
df['acceptedScientificName'] = df['apiData'].apply(lambda data: data.get('species', None))  
df['taxonomicStatusSuggested'] = df['apiData'].apply(lambda data: data.get('status', None))                                                               
df['suggestedTaxonRank'] = df['apiData'].apply(lambda data: data.get('rank', None))
df['suggestedFamily'] = df['apiData'].apply(lambda data: data.get('family', None))
df['suggestedOrder'] = df['apiData'].apply(lambda data: data.get('order', None))
df['suggestedClass'] = df['apiData'].apply(lambda data: data.get('class', None))
df['suggestedPhylum'] = df['apiData'].apply(lambda data: data.get('phylum', None))
df['suggestedKingdom'] = df['apiData'].apply(lambda data: data.get('kingdom', None))

# Función para extraer el género del canonicalName
def extract_genus(canonical_name):
    if pd.notna(canonical_name):  # Verificar que no sea NaN
        return canonical_name.split(" ")[0]  # Tomar la primera palabra como género
    else:
        return None  # Retorna None si el valor está vacío o es NaN

# Crear una nueva columna con el género extraído
df['suggestedGenus'] = df['suggestedCanonicalName'].apply(extract_genus)


#Translate the taxonRank element
df['suggestedTaxonRank']=df['taxonRank'].replace('SPECIES', 'Especie').replace('SUBSPECIES', 'Subespecie').replace('GENUS', 'Género').replace('FAMILY', 'Familia').replace('ORDER', 'Orden').replace('CLASS', 'Clase').replace('PHYLUM', 'Filo').replace('KINGDOM', 'Reino').replace('FORM', 'Forma').replace('VARIETY', 'Variedad').replace('UNRANKED', '')

 
# Función para extraer la autoría del nombre científico
def extract_authorship(row):
    scientific_name = row.get('suggestedCanonicalName', '')
    canonical_name = row.get('suggestedScientificName', '')
    
    if canonical_name and scientific_name:  # Verificar que ambos valores no sean None ni vacíos
        # Eliminar el nombre científico y obtener la autoría
        authorship = canonical_name.replace(scientific_name, '').strip()
        return authorship
    else:
        return None  # Retorna None si alguno de los valores no es válido

# Crear una nueva columna con la autoría extraída
df['scientificNameAuthorshipSuggested'] = df.apply(extract_authorship, axis=1)


#Validaciones - - REVISAR

# Función para validar si las columnas son iguales
def validate_columns(row):
    return 1 if row['scientificNameAuthorship'] == row['sscientificNameAuthorshipSuggested'] else 0

# Crear una nueva columna con la validación
df['authorshipValidation'] = df.apply(validate_columns, axis=1)


# Función para validar si las columnas son iguales
def validate_columns(row):
    return 1 if row['taxonRank'] == row['suggestedTaxonRank'] else 0

# Crear una nueva columna con la validación
df['taxonRankValidation'] = df.apply(validate_columns, axis=1)




# Seleccionar las columnas de validación
validation_columns = [
    'scientificNameValidation', 'genusValidation', 'familyValidation', 
    'orderValidation', 'classValidation', 'phylumValidation', 
    'kingdomValidation', 'taxonRankValidation', 'taxonRankValidation'
]

# Crear un resumen con el conteo de validaciones correctas (1) e incorrectas (0) por columna
validation_report = {}

for column in validation_columns:
    validation_report[column] = {
        'Correct (1)': df[column].astype(int).sum(),  # Sumar los valores 1
        'Errors (0)': len(df) - df[column].astype(int).sum()  # Calcular los valores 0
    }

# Convertir el resumen a un DataFrame para mejor visualización
validation_report_df = pd.DataFrame(validation_report).T  # Transponer para que las columnas sean índices










#Reporte de calidad: 

Qreporte = pd.merge(completeness_df, unique_values_df, on ='ElementoDwC', how="left")    

